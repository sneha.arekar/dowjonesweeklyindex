package com.index;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.index.controllers.IndexController;
import com.index.entities.IndexEntity;
import com.index.repositories.IndexRepository;

public class IndexControllerUnitTest {

	private static IndexController userController;
	private static IndexRepository mockedUserRepository;
	private static BindingResult mockedBindingResult;
	private static Model mockedModel;

	@BeforeClass
	public static void setUpUserControllerInstance() {
		mockedUserRepository = mock(IndexRepository.class);
		mockedBindingResult = mock(BindingResult.class);
		mockedModel = mock(Model.class);
		userController = new IndexController(mockedUserRepository);
	}

	@Test
	public void whenCalledshowUploadFileForm_thenCorrect() {
		IndexEntity user = new IndexEntity("1","AA","1/7/2011","$15.82","$16.72","0.182704");

		assertThat(userController.showUploadFileForm(user)).isEqualTo("upload-file");
	}

	@Test
	public void whenCalledshowAddUpForm_thenCorrect() {
		IndexEntity user =new IndexEntity("1","AA","1/7/2011","$15.82","$16.72","0.182704");
		
		assertThat(userController.showAddUpForm(mockedModel)).isEqualTo("add-index");
	}
	
	@Test
	public void whenCalledaddIndex_thenCorrect() {
		IndexEntity user =new IndexEntity("1","AA","1/7/2011","$15.82","$16.72","0.182704");

		when(mockedBindingResult.hasErrors()).thenReturn(false);

		assertThat(userController.addIndex(user, mockedBindingResult, mockedModel)).isEqualTo("add-success");
	}
	
	@Test
	public void whenCalledshowFindIndexorm_thenCorrect() {
		IndexEntity user =new IndexEntity("1","AA","1/7/2011","$15.82","$16.72","0.182704");
		
		assertThat(userController.showFindIndexorm(mockedModel)).isEqualTo("query-file");
	}
	
	

}
