package com.index.entities;

//@Entity
public class IndexEntity {

	// @Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	// private long id;
	private String quarter;
	private String stock;
	private String open;
	private String high;
	private String date;
	private String percent_return_next_dividend;

	public IndexEntity() {
	}

	public IndexEntity(String quarter, String stock, String open, String high, String date,
			String percent_return_next_dividend) {
		// super();
		this.quarter = quarter;
		this.stock = stock;
		this.open = open;
		this.high = high;
		this.date = date;
		this.percent_return_next_dividend = percent_return_next_dividend;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPercent_return_next_dividend() {
		return percent_return_next_dividend;
	}

	public void setPercent_return_next_dividend(String percent_return_next_dividend) {
		this.percent_return_next_dividend = percent_return_next_dividend;
	}

	@Override
	public String toString() {
		return "Index [quarter=" + quarter + ", stock=" + stock + ", open=" + open + ", high=" + high + ", date=" + date
				+ ", percent_return_next_dividend=" + percent_return_next_dividend + "]";
	}
}
