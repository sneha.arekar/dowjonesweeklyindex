package com.index.repositories;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.index.entities.IndexEntity;

@Repository
public interface IndexRepository extends MongoRepository<IndexEntity, Long> {

	public List<IndexEntity> findByStock(String stock);

}
