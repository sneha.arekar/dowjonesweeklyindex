package com.index.controllers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.index.entities.IndexEntity;
import com.index.repositories.IndexRepository;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Controller
public class IndexController implements ErrorController {
	private final IndexRepository indexRepository;
	private final static String PATH = "/error";

	@Override
	@RequestMapping(PATH)
	@ResponseBody
	public String getErrorPath() {
		return "No Mapping Found";
	}

	@Autowired
	public IndexController(IndexRepository indexRepository) {
		this.indexRepository = indexRepository;
	}

	@GetMapping("/")
	public String menu() {
		return "menu";
	}

	@GetMapping("/signup")
	public String showAddUpForm(Model model) {
		model.addAttribute("indices", new IndexEntity());
		return "add-index";
	}

	@GetMapping("/uploadfile")
	public String showUploadFileForm(IndexEntity indices) {
		return "upload-file";
	}

	@PostMapping("/addindex")
	public String addIndex(@ModelAttribute("indices") IndexEntity indices, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "menu";
		}

		indexRepository.save(indices);
		model.addAttribute("status", true);
		return "add-success";
	}

	@PostMapping("/upload-csv-file")
	public String uploadCSVFile(@RequestParam("file") MultipartFile file, Model model) {

		// validate file
		if (file.isEmpty()) {
			model.addAttribute("message", "Please select a CSV file to upload.");
			model.addAttribute("status", false);
		} else {

			// parse CSV file to create a list of `Indices` objects
			try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

				// create csv bean reader
				CsvToBean<IndexEntity> csvToBean = new CsvToBeanBuilder(reader).withType(IndexEntity.class)
						.withIgnoreLeadingWhiteSpace(true).build();

				// convert `CsvToBean` object to list of Indices
				List<IndexEntity> records = csvToBean.parse();

				// save Indices in DB
				indexRepository.saveAll(records);

				// fetch all customers
				System.out.println("Customers found with findAll():");
				System.out.println("-------------------------------");
				for (IndexEntity record : indexRepository.findAll()) {
					System.out.println(record);
				}
				System.out.println();
				model.addAttribute("status", true);

			} catch (Exception ex) {
				model.addAttribute("message", "An error occurred while processing the CSV file.");
				model.addAttribute("status", false);
			}
		}
		return "file-upload-status";
	}

	@GetMapping("/findIndex")
	public String showFindIndexorm(Model model) {
		model.addAttribute("stock", new IndexEntity());
		return "query-file";
	}

	@GetMapping("/query/{stock}")
	public String showQueryForm(@RequestParam(name = "stock") String stock, Model model) {
		List<IndexEntity> records = indexRepository.findByStock(stock);
		model.addAttribute("recordsup", records);
		return "display-results";
	}

}
