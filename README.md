# Project Title

This Project was created to implement a basic framework for below mentioned operations for a freelancing project. As an input dataset I have used collection of records from the [Dow Jones Index from 2011](http://archive.ics.uci.edu/ml/datasets/Dow+Jones+Index#).
This project exposes API's to allow a user to perform below mentioned operations:-

1)Upload a bulk data set (sample file present in project resources)

2)Query for data by stock ticker

3)Add a new record	

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment notes on how to deploy the project on a live system.

### Running Application with Docker

### Pre-Requisites

docker and docker-compose should be installed on your system.

Below command will create the docker image. 

Command:- 

cd ~/Project-Source-Path

docker build -t api-docker-image . 

Below command will start the application. It will create one container for database by downloading the mongodb image from docker repository and another for spring-boot app as defined in docker-compose file.

Command:-
docker-compose up 

The command issued above does the below mentioned tasks:-

1) The Dockerfile defines the app’s environment so it can be reproduced anywhere.

2)The docker-compose.yml defines the services that make up your app so that they can be run together in an isolated environment.

3)Finally running docker-compose up binds everything together and runs the entire application.	

Post running the containers in Docker,open the below URL to test the API's:-

http://localhost:9091/ 

Information about docker status can be seen from below commands:-

docker container ls

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                      NAMES

a2130923d5e2        api-docker-image    "java -Djava.securit…"   7 hours ago         Up 7 hours          0.0.0.0:9091->8080/tcp     spring-boot-crud_api_1

b2df44ea6954        mongo:3.2.4         "/entrypoint.sh --sm…"   7 hours ago         Up 7 hours          0.0.0.0:27017->27017/tcp   api-database


### Running Application without Docker

### Pre-Requisites

mongodb should be installed on your system.

After compiling the project with maven.(Remove spring.data.mongodb.host=api-database from application properties as it will use the local mongodb installation)

Command:mvn clean install

* Run com.index.Application.java as a java application
* OR
* cd ~/Project-Source-Path
* java -jar target/dowjonesweeklyindex-0.0.1-SNAPSHOT.jar

Post running open the below URL to test the API's:-

http://localhost:8080/ 

## Built With

* Spring Boot
* MongoDB
* Docker

## Nice to have

Implemented GitLab CI/CD pipeline by instructing GitLab Runner to build,test and deploy.

## AuthorsREADME.md

* **Sneha Arekar** - *Initial work* - (https://gitlab.com/sneha.arekar/dowjonesweeklyindex.git)

